# 包结构规范 (1.1)

## 简介

该文档主要的设计目标是商业体系`前端`资源分包进行约定规范，使开发资源容易被共享和复用。

### 必选字段

+ `name`: 包名。 *必须(MUST)* 为由camel命名法产生的字母组成的字符串。
+ `version`: 版本号。版本号 *必须(MUST)* 为字符串，需要符合[SemVer](http://semver.org/)的格式约定。
+ `maintainers`: 维护者列表。该字段 *必须(MUST)* 是一个数组，数组中每项 *必须(MUST)* 包含维护者的名称字段"name"与电子邮件字段"email"。

### 可选字段

+ `main`: 模块名，用来说明当前`包`的入口文件。如果包名为`foo`，那么执行`require("foo")`的时候，返回的内容就是当前模块`exports`的内容。
+ `description`: 描述信息。 *必须(MUST)* 为字符串。
+ `dependencies`: 依赖声明。该字段 *必须(MUST)* 是一个`JSON Object`，其中`key`为依赖的包名，`value`为版本号，支持如下的格式：
    + `version`
    + `>version`
    + `>=version`
    + `<version`
    + `<=version`
    + `*`: 任意版本
+ `devDependencies`: 类似`dependencies`的角色，但不是当前`包`必须的，只是在开发和调试的时候一些依赖的内容。
+ `contributors`: 贡献者列表。该字段 *必须(MUST)* 是一个数组，数组中每项 *必须(MUST)* 包含维护者的名称字段`name`与电子邮件字段`email`。
+ `homepage`: 该字段 *必须(MUST)* 为URL格式的字符串。

### 示例

 ```JavaScript
    {
        "name": "zrender",
        "version": "0.0.1",
        "maintainers": [
            {"name": "foo", "email": "foo@baidu.com"},
            {"name": "bar", "email": "bar@baidu.com", "url": "http://www.baidu.com/bar"}
        ],
        "dependencies": {
            "foo": "0.0.1",
            "bar": "*"
        },
        "devDependencies": {
            "uglify-js": "*"
        }
    }
```
