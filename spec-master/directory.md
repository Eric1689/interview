# 项目目录结构规范


## 简介

该文档主要的设计目标是项目开发的目录结构保持一致，使容易理解并方便构建与管理。

### 结构内容

├── dist/                          // build 输出目录<br/>
├── mock/                          // mock 文件所在目录，基于 express<br/>
├── models                         // 管理数据流<br/>
├── config/                        // webpack<br/>
|—— public                        //共享资源<br/>
&nbsp;&nbsp;&nbsp;&nbsp;├── index.html<br/>
└── src/<br/>                        // 源码目录<br/>
&nbsp;&nbsp;&nbsp;&nbsp;├── assets/                  // 全局静态资源 (图片、css)<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── css/basic.less       // 基础的样式定义<br/>
&nbsp;&nbsp;&nbsp;&nbsp;├── components/index.js      // 公用组件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;├── layouts/index.js        // 全局布局组件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;├── pages（views）/         // 页面目录，里面的文件即路由<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── 404.js                 // 404 页面 <br/>
&nbsp;&nbsp;&nbsp;&nbsp;├── services/                 // API接口定义<br/>
&nbsp;&nbsp;&nbsp;&nbsp;├── utils/                  // 公用工具类<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── request.js         // 接口请求<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── constant.js        // 常量文件<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;├── vertify.js         // 校验规则<br/>
├──.editorconfig - 代码风格配置，配合eslint使用<br/>
├──.eslintrc - esLint校验规则配置<br/>
├──.gitignore - 配置git忽略的文件<br/>
├──jsconfig.json - 定义项目工作空间，主要用来定义@<br/>
└── package.json<br/>
├──README.md<br/>